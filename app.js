
    const btns = Array.from(document.querySelectorAll('.btn'));
    const availableButtons = ['Enter','s', 'e', 'o', 'n', 'e', 'l', 'z']

    document.addEventListener('keyup', onKeyUp);
      
    document.addEventListener('keypress', onKeyPress);

    function onKeyUp (e) {
      if(availableButtons.includes(e.key)){
        btns.forEach((item) => {
          item.classList.remove('active-blue');
        })
      }
    }

    function onKeyPress (e) {
      if(availableButtons.includes(e.key)){
        btns.forEach((item) => {
          if( item.getAttribute('id') === e.key){
          item.classList.add('active-blue')
          }
        })
      }
    }